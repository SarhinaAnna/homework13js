const startInterval = () => {
  return setInterval(function () {
    let img = document.querySelector(".image1");
    img.classList.remove("image1");
    if (img.nextElementSibling !== null) {
      img.nextElementSibling.classList.add("image1");
    } else {
      img.parentElement.firstElementChild.classList.add("image1");
    }
  }, 3000);
};

let intervalId = startInterval();

document.getElementById("stop").addEventListener("click", function () {
  clearInterval(intervalId);
});

document.getElementById("start").addEventListener("click", function () {
  intervalId = startInterval();
});
